/* Insert regular GCC licence information here with proper exceptions. */

#undef TARGET_ORIONX
#define TARGET_ORIONX 1

/* Don't automatically add extern "C" { } around header files.  */
#undef  NO_IMPLICIT_EXTERN_C
#define NO_IMPLICIT_EXTERN_C 1

#undef LIB_SPEC
#define LIB_SPEC "-lc -lsyscall"

/* Define relevant macros for orionx. */
#undef  TARGET_OS_CPP_BUILTINS
#define TARGET_OS_CPP_BUILTINS()   \
do {                               \
  builtin_define("__orionx__");    \
  builtin_define("__unix__");      \
  builtin_assert("system=orionx"); \
  builtin_assert("system=unix");   \
  builtin_assert("system=posix");  \
} while(0);
